//
//  RAZADAppDelegate.h
//  raz-alldigital
//
//  Created by Steve Rasmussen on 4/9/14.
//  Copyright (c) 2014 eCasa, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RAZADAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
