//
//  RAZADViewController.m
//  raz-alldigital
//
//  Created by Steve Rasmussen on 4/9/14.
//  Copyright (c) 2014 eCasa, Inc. All rights reserved.
//


#import "RAZADViewController.h"
#import "Reachability.h"
#import <Parse/Parse.h>
#import "RAZADMovieListViewController.h"

@interface NSDictionary(JSONCategories)
+(NSDictionary*)dictionaryWithContentsOfJSONURLString:(NSString*)urlAddress;
-(NSData*)toJSON;
@end

@implementation NSDictionary(JSONCategories)
+(NSDictionary*)dictionaryWithContentsOfJSONURLString:(NSString*)urlAddress
{
  if ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] != NotReachable) {
    NSData* data = [NSData dataWithContentsOfURL: [NSURL URLWithString: urlAddress] ];
    __autoreleasing NSError* error = nil;
    id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    if (error != nil) return nil;
    return result;
  } else {
    // not reachable
    return nil;
  }
}

-(NSData*)toJSON
{
  NSError* error = nil;
  id result = [NSJSONSerialization dataWithJSONObject:self options:kNilOptions error:&error];
  if (error != nil) return nil;
  return result;
}
@end

@interface RAZADViewController ()

@end

@implementation RAZADViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.loadingMatchesLabel.hidden = true;
    self.loadingMatchesProgressView.hidden = true;
#ifdef ADMIN
    self.adminOptionsLabel.hidden = false;
    self.preloadParseButton.hidden = false;
#endif
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
  // search iTunes
  [self.movieSearchBar resignFirstResponder];
  self.loadingMatchesLabel.hidden = false;
  self.loadingMatchesProgressView.hidden = false;
  self.loadingMatchesProgressView.progress = 0.0;

  __weak __typeof(self) weakSelf = self;
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    // do background stuff
    NSDictionary* iTunesDict = nil;
    NSString *encodedSearchString =
      (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                            NULL,
                                                                            (CFStringRef)searchBar.text,
                                                                            NULL,
                                                                            (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                            kCFStringEncodingUTF8 ));
    
    NSString *urlString = [NSString stringWithFormat:@"https://itunes.apple.com/search?media=movie&limit=8&term=%@", encodedSearchString];
    iTunesDict =  [NSDictionary dictionaryWithContentsOfJSONURLString:urlString];
    
    if (iTunesDict) {
      // If we have a valid iTunesDict, we can cache the results
      NSArray *resultsArray = [iTunesDict objectForKey:@"results"];
      
      for (int count=0; count < [resultsArray count]; count++) {
        NSDictionary* movieDict = resultsArray[count];
        dispatch_async(dispatch_get_main_queue(), ^{
          weakSelf.loadingMatchesProgressView.progress = ((float)count/(float)[resultsArray count]);
        });
//        NSLog(@"trackName: %@", [movieDict objectForKey:@"trackName"]);
//        NSLog(@"contentAdvisoryRating: %@", [movieDict objectForKey:@"contentAdvisoryRating"]);
//        NSLog(@"artworkUrl100: %@", [movieDict objectForKey:@"artworkUrl100"]);
//        NSLog(@"trackHdPrice: %@", [movieDict objectForKey:@"trackHdPrice"]);
//        NSLog(@"longDescription: %@", [movieDict objectForKey:@"longDescription"]);
//        NSLog(@"trackId: %@", [movieDict objectForKey:@"trackId"]);
        
        // See if we have cached the unique trackId
        PFQuery *query = [PFQuery queryWithClassName:@"Movie"];
        query.cachePolicy = kPFCachePolicyCacheElseNetwork;
        [query whereKey:@"trackId" hasPrefix:[NSString stringWithFormat:@"%@",[movieDict objectForKey:@"trackId"]]];
        PFObject* object = [query getFirstObject];
          if (!object) {
            NSLog(@"The getFirstObject request failed.");
            // If not cached, save it
            PFObject *movie = [PFObject objectWithClassName:@"Movie"];
            if ([movieDict objectForKey:@"trackName"]) {
              movie[@"trackName"] = [movieDict objectForKey:@"trackName"];
              movie[@"lowercaseTrackName"] = [[movieDict objectForKey:@"trackName"] lowercaseString];
            }
            if ([movieDict objectForKey:@"releaseDate"]) {
              movie[@"releaseDate"] = [movieDict objectForKey:@"releaseDate"];
            }
            if ([movieDict objectForKey:@"contentAdvisoryRating"]) {
              movie[@"contentAdvisoryRating"] = [movieDict objectForKey:@"contentAdvisoryRating"];
            }
            if ([movieDict objectForKey:@"trackViewUrl"]) {
              movie[@"trackViewUrl"] = [movieDict objectForKey:@"trackViewUrl"];
            }
            if ([movieDict objectForKey:@"artworkUrl100"]) {
              movie[@"artworkUrl100"] = [movieDict objectForKey:@"artworkUrl100"];
            }
            if ([movieDict objectForKey:@"artworkUrl60"]) {
              movie[@"artworkUrl60"] = [movieDict objectForKey:@"artworkUrl60"];
            }
            if ([movieDict objectForKey:@"trackPrice"]) {
              movie[@"trackPrice"] = [movieDict objectForKey:@"trackPrice"];
            }
            if ([movieDict objectForKey:@"longDescription"]) {
              movie[@"longDescription"] = [movieDict objectForKey:@"longDescription"];
            } else {
              movie[@"longDescription"] = @"Long Description not available from iTunes";
            }
            if ([movieDict objectForKey:@"trackId"]) {
              movie[@"trackId"] = [NSString stringWithFormat:@"%@",[movieDict objectForKey:@"trackId"]];
            }
            NSCharacterSet *charactersToRemove =
            [[ NSCharacterSet alphanumericCharacterSet ] invertedSet ];
            NSString *trimmedReplacement = [[[[movieDict objectForKey:@"trackName"] lowercaseString] componentsSeparatedByCharactersInSet:charactersToRemove ]componentsJoinedByString:@" "];
            NSArray * searchTermsArray = [trimmedReplacement componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            movie[@"searchTermsArray"] = searchTermsArray;
            [movie save];
            // Update local cache
            [movie refresh];
          } else {
            // The find succeeded.
//            NSLog(@"Successfully retrieved the movie object. %@", object);
          }
       }
     }
      dispatch_async(dispatch_get_main_queue(), ^{
      // do main-queue stuff, like updating the UI
//        NSLog(@"myInfo: %@", iTunesDict);
        NSLog(@"Movie: %@", searchBar.text);
        
        weakSelf.loadingMatchesLabel.hidden = true;
        weakSelf.loadingMatchesProgressView.hidden = true;
        weakSelf.loadingMatchesProgressView.progress = 0.0;
        [weakSelf performSegueWithIdentifier:@"pushMovieList" sender:[NSString stringWithString:searchBar.text]];
    });
  });
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
  [self.movieSearchBar resignFirstResponder];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  NSString *identifier = [segue identifier];
  if ([identifier isEqualToString:@"pushMovieList"]) {
    RAZADMovieListViewController *controller = [segue destinationViewController];
    controller.searchString = sender;
  }
}



#ifdef ADMIN
// Admin Section
- (IBAction)preloadParse:(id)sender {
    
}
#endif


@end
