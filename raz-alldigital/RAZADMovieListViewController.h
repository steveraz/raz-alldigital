//
//  RAZADMovieListVCViewController.h
//  raz-alldigital
//
//  Created by Steve Rasmussen on 4/10/14.
//  Copyright (c) 2014 eCasa, Inc. All rights reserved.
//

#import <Parse/Parse.h>

@interface RAZADMovieListViewController : PFQueryTableViewController

@property (strong, nonatomic)  NSString *searchString;

@end
