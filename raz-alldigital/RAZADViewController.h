//
//  RAZADViewController.h
//  raz-alldigital
//
//  Created by Steve Rasmussen on 4/9/14.
//  Copyright (c) 2014 eCasa, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RAZADViewController : UIViewController <UISearchBarDelegate>
@property (strong, nonatomic) IBOutlet UIButton *preloadParseButton;
@property (strong, nonatomic) IBOutlet UILabel *adminOptionsLabel;
@property (strong, nonatomic) IBOutlet UISearchBar *movieSearchBar;
@property (strong, nonatomic) IBOutlet UILabel *loadingMatchesLabel;
@property (strong, nonatomic) IBOutlet UIProgressView *loadingMatchesProgressView;

@end
