//
//  RAZADMovieListVCViewController.m
//  raz-alldigital
//
//  Created by Steve Rasmussen on 4/10/14.
//  Copyright (c) 2014 eCasa, Inc. All rights reserved.
//

#import "RAZADMovieListViewController.h"
#import "RAZADMovieDetailViewController.h"

@interface RAZADMovieListViewController ()

@end

@implementation RAZADMovieListViewController

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//      self.searchString = [NSString string];
//    }
//    return self;
//}

- (id)initWithCoder:(NSCoder *)aDecoder
{
  self = [super initWithClassName:@"Movie"];
  self = [super initWithCoder:aDecoder];
  if (self) {
    // The className to query on
    self.parseClassName = @"Movie";
    
    // The key of the PFObject to display in the label of the default cell style
    self.textKey = @"trackName";
    
    // Whether the built-in pull-to-refresh is enabled
    self.pullToRefreshEnabled = YES;
    
    // Whether the built-in pagination is enabled
    self.paginationEnabled = NO;
    
    // The number of objects to show per page
    //self.objectsPerPage = 25;
    
    /// The key to use to display for the cell image view.
//    self.imageKey = @"artwirlUrl100";
    
    // Show isLoading
    self.isLoading = YES;
    
    self.searchString = [NSString string];
  }
  return self;
}


- (PFQuery *)queryForTable{
  PFQuery *query = [PFQuery queryWithClassName:@"Movie"];
  query.cachePolicy = kPFCachePolicyCacheElseNetwork;
//  [query whereKey:@"lowercaseTrackName" containsString:[self.searchString lowercaseString]];
  NSCharacterSet *charactersToRemove =
  [[ NSCharacterSet alphanumericCharacterSet ] invertedSet ];
  NSString *trimmedReplacement = [[self.searchString componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@" "];
  NSArray * searchTermsArray = [trimmedReplacement componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
  [query whereKey:@"searchTermsArray" containedIn:searchTermsArray];
  return query;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  NSString *identifier = [segue identifier];
  if ([identifier isEqualToString:@"pushMovieDetails"]) {
    RAZADMovieDetailViewController *controller = [segue destinationViewController];
    NSInteger row = [[self tableView].indexPathForSelectedRow row];
    controller.movieDetailPFObject = [self.objects objectAtIndex:row];
  }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [super tableView:tableView didSelectRowAtIndexPath:indexPath];
  [self performSegueWithIdentifier:@"pushMovieDetails" sender:indexPath];
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//  return 44.0;
//}


//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
//  
//  static NSString *CellIdentifier = @"Cell";
//  
//  PFTableViewCell *cell = (PFTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//  if (cell == nil) {
//    cell = [[PFTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//  }
//  
//  // Configure the cell
//  cell.textLabel.text = [object objectForKey:self.textKey];
////  cell.imageView.file = [object objectForKey:self.imageKey];
////  cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",[object objectForKey:@"contentAdvisoryRating"]];
//  cell.textLabel.text = @"textLabel";
//  cell.detailTextLabel.text = @"detailTextLabel";
// 
//  // Load image in background, display when ready
////  dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
////  dispatch_async(myQueue, ^{
////    // Perform long running process
////    cell.imageView.contentMode = UIViewContentModeCenter;
////    
////    UIImage *backgoundFetechedUIImage = [UIImage imageWithData:
////                                         [NSData dataWithContentsOfURL:
////                                          [NSURL URLWithString: [object objectForKey:@"artworkUrl100"]]]];
////    dispatch_async(dispatch_get_main_queue(), ^{
////      // Update the UI
////      cell.imageView.image = backgoundFetechedUIImage;
////    });
////  });
//
//
//  return cell;
//}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
  static NSString *CellIdentifier = @"MovieCell";
  
  PFTableViewCell *cell = (PFTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  if (cell == nil) {
    cell = [[PFTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
  }
  
  // Configure the cell
  cell.textLabel.adjustsFontSizeToFitWidth = YES;
  cell.textLabel.text = [NSString stringWithFormat:@"%@ ",[object objectForKey:self.textKey]];

  cell.detailTextLabel.text = [object objectForKey:@"contentAdvisoryRating"];

  cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
  
  cell.imageView.image = [UIImage imageWithData:
                                           [NSData dataWithContentsOfURL:
                                           [NSURL URLWithString: [object objectForKey:@"artworkUrl60"]]]];

  
  return cell;
}


@end
