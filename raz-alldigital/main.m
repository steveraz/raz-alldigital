//
//  main.m
//  raz-alldigital
//
//  Created by Steve Rasmussen on 4/9/14.
//  Copyright (c) 2014 eCasa, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RAZADAppDelegate.h"

int main(int argc, char * argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([RAZADAppDelegate class]));
  }
}
