//
//  RAZADovieDetailViewController.h
//  raz-alldigital
//
//  Created by Steve Rasmussen on 4/13/14.
//  Copyright (c) 2014 eCasa, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface RAZADMovieDetailViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *movieImageView;
@property (strong, nonatomic) IBOutlet UILabel *moviePriceLabel;
@property (strong, nonatomic) IBOutlet UILabel *movieNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *movieRartingLabel;
@property (strong, nonatomic) IBOutlet UILabel *movieReleaseLabel;
@property (strong, nonatomic) IBOutlet UITextView *movieDescriptionTextView;
@property (strong, nonatomic) IBOutlet PFObject *movieDetailPFObject;

@end
