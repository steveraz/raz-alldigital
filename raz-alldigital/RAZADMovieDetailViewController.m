//
//  RAZADovieDetailViewController.m
//  raz-alldigital
//
//  Created by Steve Rasmussen on 4/13/14.
//  Copyright (c) 2014 eCasa, Inc. All rights reserved.
//

#import "RAZADMovieDetailViewController.h"

@interface RAZADMovieDetailViewController ()

@end

@implementation RAZADMovieDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
      self.movieDetailPFObject = [[PFObject alloc] init];
    }
    return self;
}


- (NSString *)userVisibleDateTimeStringForRFC3339DateTimeString:(NSString *)rfc3339DateTimeString {
  /*
   Returns a user-visible date time string that corresponds to the specified
   RFC 3339 date time string. Note that this does not handle all possible
   RFC 3339 date time strings, just one of the most common styles.
   */
  
  NSDateFormatter *rfc3339DateFormatter = [[NSDateFormatter alloc] init];
  NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
  
  [rfc3339DateFormatter setLocale:enUSPOSIXLocale];
  [rfc3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
  [rfc3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
  
  // Convert the RFC 3339 date time string to an NSDate.
  NSDate *date = [rfc3339DateFormatter dateFromString:rfc3339DateTimeString];
  
  NSString *userVisibleDateTimeString;
  if (date != nil) {
    // Convert the date object to a user-visible date string.
    NSDateFormatter *userVisibleDateFormatter = [[NSDateFormatter alloc] init];
    assert(userVisibleDateFormatter != nil);
    
    [userVisibleDateFormatter setDateStyle:NSDateFormatterShortStyle];
    [userVisibleDateFormatter setTimeStyle:NSDateFormatterNoStyle];
    
    userVisibleDateTimeString = [userVisibleDateFormatter stringFromDate:date];
  }
  return userVisibleDateTimeString;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.movieNameLabel.text = [self.movieDetailPFObject objectForKey:@"trackName"];
    float floatPrice = [[self.movieDetailPFObject objectForKey:@"trackPrice"] floatValue];
    self.moviePriceLabel.text = [NSString stringWithFormat:@"$%.02f",floatPrice];
    self.movieRartingLabel.text = [NSString stringWithFormat:@"Rating: %@",[self.movieDetailPFObject objectForKey:@"contentAdvisoryRating"]];
    self.movieDescriptionTextView.text = [self.movieDetailPFObject objectForKey:@"longDescription"];
  
    // Parse out date sent by Apple
    NSString *formattedDateString = [self userVisibleDateTimeStringForRFC3339DateTimeString:[self.movieDetailPFObject objectForKey:@"releaseDate"]];
    self.movieReleaseLabel.text = [NSString stringWithFormat:@"%@",formattedDateString];

    // Load image in background, display when ready
    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    dispatch_async(myQueue, ^{
      // Perform long running process
      UIImage *backgoundFetechedUIImage = [UIImage imageWithData:
                                         [NSData dataWithContentsOfURL:
                                          [NSURL URLWithString: [self.movieDetailPFObject objectForKey:@"artworkUrl100"]]]];
      dispatch_async(dispatch_get_main_queue(), ^{
        // Update the UI
        self.movieImageView.image = backgoundFetechedUIImage;
      });
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)tapiTunesButton:(id)sender {
  NSString *iTunesLink = [self.movieDetailPFObject objectForKey:@"trackViewUrl"];
  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
